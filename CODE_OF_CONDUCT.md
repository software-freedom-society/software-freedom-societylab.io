# Software Freedom Society Code of Conduct

As members of this society, and in the interest of fostering an
open and welcoming community, we pledge to respect all people who contribute through
reporting issues, posting feature requests, updating documentation, submitting pull
requests or patches, and other activities.

We are committed to making participation in this project a harassment-free experience
for everyone, regardless of level of experience, gender, disability, personal appearance,
body size, race, ethnicity, age, religion, or nationality.

Examples of unacceptable behavior by participants include:

* The use of vulgar language or imagery
* Personal attacks
* Trolling or insulting/derogatory comments
* Public or private harassment
* Other unethical or unprofessional conduct

Society members have the right and responsibility to remove, edit, or reject comments,
commits, code, wiki edits, issues, and other contributions that are not aligned
to this Code of Conduct. By adopting this Code of Conduct, society members commit
themselves to fairly and consistently applying these principles to every aspect
of managing this project. Society members who do not follow or enforce this
Code of Conduct may be permanently removed from leadership roles.

This code of conduct applies both internally and in public; basically, whenever
an individual is representing the society.

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported
by opening an issue or contacting one or more of the society members.

This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org),
version 1.2.0, available at https://www.contributor-covenant.org/version/1/2/0/code-of-conduct.html
