# Welcome

**"Let your light so shine before men, that they may see your good works, and glorify your Father which is in heaven" ([Matt. 5:16][1]).**

The Software Freedom Society is a [student-led][2] [club][3] devoted to the development of [Free Software][4]!

## Updates

<iframe class="nav" style="width: 100%; height: 400px;" allowfullscreen sandbox="allow-top-navigation allow-scripts" src="https://www.mastofeed.com/api/feed?url=https%3A%2F%2Fmastodon.host%2Fusers%2FSoftwareFreedomSociety.atom&theme=light&size=100&header=true&replies=true&boosts=true"></iframe>

[1]: https://en.wikisource.org/wiki/Bible_(King_James)/Matthew#5:16
[2]: https://gitlab.com/software-freedom-society/software-freedom-society.gitlab.io/raw/master/AUTHORS.txt
[3]: http://www.liberty.edu/index.cfm?PID=19868
[4]: https://www.gnu.org/philosophy/free-sw.html
