# Get Involved

## Getting Started with Free/Libre and Open Source Software (FLOSS)

- [Getting Involved in Open Source Projects](https://blog.teamtreehouse.com/getting-involved-open-source-projects) - Open source is a great way to gain experience and meet new people in the tech industry.

- [How to Contribute to Open Source](https://opensource.guide/how-to-contribute) - A guide to making open source contributions, for first-timers and for veterans.

- [6 Easy Ways To Get Started Programming Open Source](http://www.grokcode.com/108/6-easy-ways-to-get-started-programming-open-source) - Open source projects can be a good way to geek out and do what you love...

- [First Contributions](https://firstcontributions.github.io) - Make your first open source contribution in 5 minutes.

- [First Timers Only](https://www.firsttimersonly.com) - Get involved in Open Source and commit code to your first project! 

- [OpenHatch](http://openhatch.org) - OpenHatch is a non-profit dedicated to matching prospective free software contributors with communities, tools, and education.

## Featured Organizations

The software development ideologies practiced by the Software Freedom Society are exemplified in the following organizations.

### Framasoft

[Framasoft](https://framasoft.org/en/)

"There is a long way to go, but the road is Free..."

### Free Software Foundation

[Free Software Foundation](https://www.fsf.org)

A nonprofit with a worldwide mission to promote computer user freedom. We defend the rights of all software users.

### Electronic Frontier Foundation

[Electronic Frontier Foundation](https://www.eff.org)

The leading nonprofit defending digital privacy, free speech, and innovation.

### Software Freedom Conservancy

[Software Freedom Conservancy](https://sfconservancy.org)

A not-for-profit charity that helps promote, improve, develop, and defend Free, Libre, and Open Source Software (FLOSS) projects.
