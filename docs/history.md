# History

The Software Freedom Society is an **unofficial** club of [Liberty University][1];
We are not currently affiliated with the institution in anyway.

## Background

### Club Founded

The Software Freedom Society (SFS), was originally started by Peter Kohler, for students attending Liberty University. 
In 2016, the [domain][2] "libertysfs.com", was purchased and the club began hosting several services—a website, wiki, 
[git][3] repositories, and a [nextcloud][4] instance (Kohler and Stacey, 2018).

### President steps down

In 2017, Peter stepped down as President, and Luke Stacey stepped up to take his place (Kohler, 2018). 
Tim Magee also served the club as treasurer (Stacey, 2018).

### Online student seeks membership

In the Summer of 2018, Lucas Ramage, transferred to Liberty as a full-time online student.
Taking classes online allowed him to finish his degree, while working full-time and spending time with his family.
Other than discussion posts, and occasional emails with his teachers, he really didn't feel connected to the student body,
or with the school. Liberty University provides clubs so that students can interact with others that share their passions ("Clubs & Student Organizations", 2018), 
so he sought to join this club in hopes of meeting other students that were interested in writing [free software][5],

### SFS shuts down

Luke and Tim had already decided to close the club due to a lack of interest, but were willing to
pass the club to any other residential student who would take ownership of it (Stacey, 2018). Unfortunately, 
Lucas was an online-only student who lived in Florida. This did not sit well with him at all. Free Software is built by global, 
online communities like the [Apache Foundation][6], [Free Software Foundation][7], [Mozilla][8]. Some of the most influencial 
software in the world is developed on the world wide web, so why shouldn't a university club devoted to fostering free software also take place online?

### Online student turned away

Emails were exchanged between Lucas and the previous club leadership throughout the summer. Jenna Parisen, Director of Clubs, 
and Jacob Page, Student Body President, were also included in the discussion. Ultimately, it was decided that
the SFS could not be led by an online student since the leadership are expected to physically attend meetings on-campus
throughout the year (Parisen, 2018). It was also noted that official clubs are required to have a faculty sponsor, which
must meet with the club leadership as well (Page, 2018).

## Recent Developments

### ["Diligence is the mother of good fortune"][11]

Fortunately, both previous club presidents have given their blessing for the club to continue, albeit unofficially, until a sizeable online presence has been garnered enough to warrant a change of policy within the institution.

From Peter, "It is a pity that online clubs are not presently recognized; I hope that you succeed in raising awareness regarding that" (Kohler and Stacey, 2018).

From Luke, "I'm just glad to see someone keeping the FOSS flame on fire" (Kohler and Stacey, 2018).

## References

“Clubs & Student Organizations.” The Liberty Champion, Liberty University Online, [www.liberty.edu/index.cfm?PID=19868][9]. Accessed 25 July 2018.

Kohler, Peter. “Re: Software Freedom Society” Message to Lucas Ramage. 18 May 2018. E-mail.

Kohler, Peter and Stacey, Luke. “Domain Name Transfer” Message to Lucas Ramage. 18 May 2018. E-mail.

Liberty University Student Government Association. [Club and Student Organization Policy Handbook][10]. Office of Club Administration, 2015.

Page, Jacob. “Re: Software Freedom Society” Message to Lucas Ramage. 21 June 2018. E-mail.

Parisen, Jenna. “Re: Software Freedom Society” Message to Lucas Ramage. 18 June 2018. E-mail.

Stacey, Luke. “Re: Software Freedom Society” Message to Lucas Ramage. 22 May 2018. E-mail.

[1]: https://www.liberty.edu
[2]: https://en.wikipedia.org/wiki/Domain_name
[3]: https://git-scm.com
[4]: https://nextcloud.com
[5]: https://www.gnu.org/philosophy/free-sw.html
[6]: http://apache.org
[7]: https://www.fsf.org
[8]: https://www.mozilla.org
[9]: https://www.liberty.edu/index.cfm?PID=19868
[10]: http://www.liberty.edu/media/1222/clubsadmin/ClubPolicyHandbook.docx.pdf
[11]: https://en.wikiquote.org/wiki/Diligence
