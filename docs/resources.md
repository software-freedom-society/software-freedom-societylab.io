# Further Resources

## Books

- [Invent with Python](https://inventwithpython.com)
- [Programming Languages: Application and Interpretation](https://www.plai.org)
- [Python For Everybody: Exploring Data In Python](https://www.py4e.com/book)

## Certifications

- [Creative Commons Certificate](https://certificates.creativecommons.org)

## Curriculums

- [The Open Source Computer Science Degree](https://github.com/ForrestKnight/open-source-cs)
- [Open Source Society University](https://github.com/ossu/computer-science)

## Internships

- [Google Summer of Code](https://summerofcode.withgoogle.com)
- [Google Season of Docs](https://developers.google.com/season-of-docs)
- [Outreachy](https://www.outreachy.org/communities/cfp)

## Colleges

- [Case Western Reserve University Hacker Society](http://hacsoc.org)

- [Oregon State University (OSU) Open Source Lab](https://osuosl.org)

- [University of Florida Open Source Club](https://ufopensource.club)

## Miscellaneous

- [Coding for Interviews](http://codingforinterviews.com)