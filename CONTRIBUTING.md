# Contributing to the Software Freedom Society

An introduction to contributing to the Software Freedom Society.

The Software Freedom Society welcomes, and depends, on contributions from students and
collaborators in the open source and academic communities. Contributions can be made in a number of
ways, a few examples are:

- Code patches via pull requests
- Documentation improvements
- Bug reports and patch reviews

## Code of Conduct

Everyone interacting in the Software Freedom Society's codebases, issue trackers, chat
rooms, and mailing lists is expected to follow the [SFS Code of Conduct].

## Reporting an Issue

Please include as much detail as you can. Let us know your platform and relevant
version numbers for the project in question. If the problem is visual (for example a theme or design issue)
please add a screenshot and if you get an error please include the full error and traceback.

## Submitting Pull Requests

Once you are happy with your changes or you are ready for some feedback, push
it to your fork and send a pull request. For a change to be accepted it will
most likely need to have tests and documentation if it is a new feature.

## Resources

[Contributing to Open Source Projects](http://www.contribution-guide.org)

[How To Ask Questions The Smart Way](http://www.catb.org/esr/faqs/smart-questions.html)

[Further Resources](docs/resources.md)

[SFS Code of Conduct]: CODE_OF_CONDUCT.md